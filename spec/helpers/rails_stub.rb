#!/usr/bin/env ruby

require "pathname"

$LOAD_PATH << Pathname.new(Dir.getwd).join("lib").expand_path.to_path
file = Pathname.new(Dir.getwd).join(ARGV.shift).expand_path.to_path

require_relative file
