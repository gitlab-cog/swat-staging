require "cog"
require "swat"
require "rails_loader"

module CogCmd
  module Swat
    #
    # Cog Command that loads and dryruns the given script
    #
    class Dryrun < Cog::Command
      def run_command
        rails = ::Swat::RailsLoader.new
        response.template = "execution_result"
        response.content = rails.run("dryrun #{request.args.join(' ')}")
      end
    end
  end
end
